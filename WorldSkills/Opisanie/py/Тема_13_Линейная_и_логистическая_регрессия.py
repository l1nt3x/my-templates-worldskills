# %%
!git clone https://github.com/neuralcomputer/ML_School.git

# %%
"""
# Тема № 13. Линейная и логистическая регрессия
## Модель линейной регрессии
Модели машинного обучения устанавливают некоторую зависимость выходов от входов, т.е. задают правила, как посчитать выход, зная вход и параметры модели. В зависимости от того, каковы эти правила, различают и разные модели.

Одни из самых простых правил - **линейные**.

Линейные модели задают зависимости вида: 

(y - выход, x- вход, a и b - параметры)

\\(y=a*x+b\\)

Здесь вход x умножается на параметр a и к этому прибавляется параметр b.

Вход и выход могут быть векторами. Пусть х и y - вектора, в этом случае зависимость похожа, но теперь A это матрица параметров, а b - вектор и производится *матричное* умножение. Как вы помните, при матричном умножении необходимо соблюдать размерность, количество столбцов в матрице А равно количеству элементов в векторе х, а количество строк в матрице А равно количеству элементов в векторе у и равно количеству элементов в векторе b.

$ \overrightarrow{y}=A*\overrightarrow{x}+\overrightarrow{b} \\ \
\\
\overrightarrow{x} - вектор\ размерности\ N \\ 
\overrightarrow{y} - вектор\ размерности\ M \\ 
\overrightarrow{b} - вектор\ размерности\ M \\ 
A - матрица\ размерности\ M*N \\ $


Линейные зависимости очень просты, их легко считать, линейные модели легко обучать, но - всегда есть но - они хороши только для простых данных с почти линейными зависимостями, а такое бывает очень редко. Все же линейная модель это первое, что стоит попробовать при решении задачи, часто они используются как начальный вариант с другими моделями. Модели, которые не являются линейными, называются **нелинейными**, с ними познакомимся в другой раз.

Следует указывать также, по какой переменной модель линейна, может быть так, что она линейна по входу, но не по параметрам, а может наоборот. Линейная регрессия линейна и по входу и по параметрам.
"""

# %%
"""
## Обучение линейной регрессии. Итерационный метод наименьших квадратов.
Мы задали модель, теперь ее нужно обучить. Обучить - значит подобрать параметры так, чтобы ошибка была как можно меньше. Нужно задать и посчитать ошибку.

Много способов придумано для расчета ошибки, пожалуй самый распространенный из них это **метод наименьших квадратов** (МНК). 

В методе наименьших квадратов ищут такие параметры, которые дали бы наименьшее значение суммы квадратов расстояний выходов модели от заданных (указаний учителя) на заданных входах. Такая длинная и непонятная фраза, давайте посмотрим на одномерном примере.

Задано **P** примеров данных (синие точки): вход **х** и желаемый выход (указания учителя) **t**.

![img](https://drive.google.com/uc?id=1dFaEsDIqbufA3VgIlMfZ4kyi-9K7h5aW)

Если мы выбрали какие-то параметры **a** и **b**, то для любого входа **x** можем посчитать выход модели **y**. Как вы можете догадаться это будет прямая линия (зеленая).

В идеальном случае выход **y** для заданного входа **х** должен совпасть с указанием учителя **t** для этого же входа. А если не совпадает, то модель ошибается. Мы можем посчитать насколько именно ошибается модель (красная пунктирная линия). Ошибка это разница между тем что получилось и тем что должно было получиться на выходе \\(e(x)=y(x)-t(x)\\). Для каждого входа **х** эта ошибка своя, попытаемся уменьшить ее для одного входа - может увеличиться для другого. Надо сделать так, чтобы для всех входов сразу общая ошибка была как можно меньше. Но обратите внимание, что такая ошибка имеет знак. Если мы просто сложим ошибки разного знака (+5 и -5 например) то суммарная будет 0, но модель не угадает выходы: на одном она ошиблась в одну сторону (+5), на другом - в другую (-5). Поэтому нужно использовать не саму ошибку, а ее абсолютное значение, модуль. Модуль не очень приятная функция, она не дифференцируемая в нуле, лучше вместо модуля использовать квадрат модуля! А вот тут все прекрасно, квадрат модуля дифференцируемая функция, считается не так уж сложно и неотрицательна. Причем равняется нулю, только если аргумент равен нулю. 

Итак, будем использовать функцию ошибки: сумма по всем примерам **P** квадратов разности действительного выхода **y** и желаемого выхода **t** на входных примерах **x**. 


$ E=\sum_{i=1}^{P} e(x_i)^2=\sum_{i=1}^{P} (y(x_i)-t(x_i))^2\\
x_i : i-ый\ пример\ входа\\
t(x_i) : указание\ учителя\ для\ него\\
y(x_i) : выход\ модели\ для\ него \\ $

Ради математической красоты можно поделить это на 2 и на P, но мы не будем, нас волнует не конкретное значение функции ошибки, а где находится ее минимум, а умножением и делением на константы мы положение минимума не меняем.

Мы явно не написали, но функция ошибки зависит от параметров модели. Почему? Потому что от них зависит выход **y**. Изменяя параметры модели можем найти такие, для которых ошибка будет наименьшей. Отсюда и название метода наименьших квадратов - функция ошибки это сумма квадратов чего-то там, и мы ищем наименьшее ее значение.
"""

# %%
"""
 
### Градиентный спуск.
Осталось найти параметры, для которых функция ошибки минимальна. Много разных методов есть для этого. 

Например, перебор: изменяем параметры, смотрим уменьшилась ли функция ошибки, если уменьшилась - запоминаем эти значения параметров, если нет - не запоминаем. И так перебираем все возможные значения параметров. Метод хороший, если у нас мало возможных значений параметров, но такое бывает крайне редко. Ну-ка посчитайте, если есть два параметра, каждый может принимать 10 значений, то сколько всего вариантов комбинаций параметров может быть? 100? Правильно, сто это не так много, переберем. А если три таких параметра? И тысячу переберем. А если таких параметров всего-то 100, сколько вариантов перебрать надо? Ой-ой, по-гуглите, есть ли у такого числа название. Даже если с начала зарождения вселенной каждую наносекунду по одному варианту перебирать, то так все и не переберем, вот проклятье! Такая ситуация, что количество вариантов перебора растет очень-очень быстро так и называется **проклятие размерности**. Нет, переборы нам ненужны.

Ну ладно, другой вариант: Если присмотреться к функции ошибки, то поймем, что от параметров она зависит квадратично. Всего-то нужно решить квадратное уравнение. И это можно сделать. Но мы не будем. Все хорошо только потому, что функция ошибки квадратична по параметрам. Это прекрасное свойство для линейных моделей, но вот для нелинейных это не так. Хотелось бы все-таки метод, который к любым, ну или большинству, моделям подходил.

И такой метод есть - это **градиентный спуск**, нет, лучше, **ГРАДИЕНТНЫЙ СПУСК**. Большие буквы заслужены, этот метод является основой основ машинного обучения, ему мы обязаны современными чудесными приложениями. Чтобы понять суть метода нужно вспомнить, что такое **производная**. 

Представьте, что вы стоите на горе с закрытыми глазами. Вам надо спуститься вниз. Вы не видите где этот низ расположен, но чувствуете, куда поверхность опускается, и можете понять, идти ли вам направо или налево, чтобы спускаться. Ах, вот если бы для функции, не видя ее графика, можно было понять, она увеличивается или уменьшается в конкретной точке. Но как раз это и показывает производная функции. Если производная больше нуля - функция возрастает, если меньше нуля - убывает, а если ноль - то не изменяется с ростом аргумента. Нам нужен минимум функции ошибки, значит двигаться надо в сторону ее убывания. Вы, стоя на горе, можете сделать маленький шаг в направлении низа, и мы можем немножко изменить параметр модели в том направлении, куда функция ошибки убывает. А потом еще раз, и еще раз, и еще. Дойдем до низа, производная станет равна нулю. Если перескочим (ну вот такие длинные шаги), сразу почувствуем что направление изменилось и низ с другой стороны, а производная изменит знак. 

Все что нам нужно сделать, так это считать производную функции ошибки по параметру и изменять его, постепенно увеличивая или уменьшая в зависимости от знака (и величины) производной. В многомерном случае, когда параметров много, нам нужно считать производные функции ошибки по каждому из них, 100 параметров - 100 производных посчитать надо. Вектор, составленный из производных по каждому параметру называют **градиентом**. 

Итак, в методе градиентного спуска мы считаем вектор градиента и изменяем вектор параметров в направлении антиградиента (т.е. "минус" градиента). Так делаем несколько раз *итеративно* (в цикле).
Пусть **t** - номер текущей итерации, **w(t)** - параметр модели, на этой итерации , **E(t)** - функция ошибки на этой итерации, зависящая от всех параметров, входов, и указаний учителя, тогда на следующей итерации **t+1** параметр в методе градиентного спуска задается как:

\\(w(t+1)=w(t)-s*\frac{\partial E(t)}{\partial w}\\)

то есть новое значение параметра равно текущему значения *минус* производная функция ошибки по этому параметру умноженная на коэффициент **s**. Минус потому, что нужно в сторону уменьшения функции ошибки двигаться. Коэффициент **s** (вместе с величиной производной) определяет длину шага, как сильно мы изменяем параметр. Его задают вручную, но есть и методы для автоматического подбора шага. Для краткости называют его "шаг" обучения (learning rate).

Это очень простой метод, но в нем есть много подводных камней, мы про них поговорим отдельно на других занятиях, пока попробуйте самостоятельно понять какие трудности с ним могут возникнуть.    
"""

# %%
"""
### Итерационный МНК.
Но вернемся к линейной регрессии. Для нее функция ошибки квадратична по параметрам и посчитать производную легко. В нашем примере мы имеем два параметра **a** и **b**. Легко посчитать, что тогда:
* производная по **a** равна \\( \frac{\partial E}{\partial a}= 2 * \sum_{i=1}^{P} (y(x_i)-t(x_i)) * x_i\\)
* производная по **b** равна \\( \frac{\partial E}{\partial b}= 2 * \sum_{i=1}^{P} (y(x_i)-t(x_i))\\)

И правда, производная суммы равна сумме производных. Внутри суммы квадрат. Производная от квадрата чего-либо это два умножить на это что-либо. В скобочках выход минус указание учителя. Указание учителя от параметра не зависит, значит для него производная ноль. Остается выход y. Но выход это произведение входа **x** на параметр **a** плюс параметр **b**. Значит для него производная по **a** будет равна **x**, а по  **b** будет равна 1, потому что вход от параметров не зависит и параметры друг от друга не зависят. 

Теперь мы знаем как обучить линейную регрессию, давайте это и сделаем. Сначала сделаем программу для расчета сами, а в будущем будем пользоваться реализацией из библиотек.
"""

# %%
"""
### Генерация данных
Сгенерируем данные, которые похожи на линейную зависимость, но все же не точно линейны.
"""

# %%
import numpy as np # подключим библиотеку для работы с массивами 

np.random.seed(42) # начало генератора случайных чисел, чтобы после перезапуска были одинаковые

x = np.random.randn(1, 100) #(100, 1) # генерируем случайно несколько примеров входа, одномерный.
a, b = 2, 1 # коэффициенты зависимости в данных
eps = .1 * np.random.randn(1, 100) # шум с нормальным распределением
y = b + a*x + eps # примеры выхода

x.shape, y.shape

# %%
"""
Перемешаем и разделим примеры на тестовые и обучающие.
"""

# %%
new_ind = np.arange(100) #сгенерируем массив последовательных чисел по количесвту данных - индексы данных

np.random.shuffle(new_ind)# случайно перемешаем их (переставим элементы массива)
new_ind #

# %%
train_idx = new_ind[:70]# первую часть индексов (с 0 до 69) определим как обучающие
test_idx = new_ind[70:]# вторую часть индексов (с 70 до конца=99) определим как тестовые
# используя эти индексы разделим массивы данных на обучающие и тестовые
x_train, y_train = x[0][train_idx], y[0][train_idx]# обучающие вход и желаемый выход
x_test, y_test = x[0][test_idx], y[0][test_idx]# тестовые вход и желаемый выход


# %%
"""
Посмотрим на полученные данные. Мы используем ранее написанную функцию `plot_line_or_scatter` для отрисовки. Мы сохранили ее в файл utils.py из которого ее и можно подключить. Функция основана на matplotlib и в файле можно посмотреть на ее реализацию.

Зависимость между входом **x** и выходом **y** получилась почти линейная, как мы и хотели.
"""

# %%
from ML_School.utils import plot_line_or_scatter as plt_my # подключаем функцию для рисования
plt_my('scatter', x_train, y_train, color='green') # рисуем обучающие данные, зависимость желаемого выхода от входа

# %%
"""
### Обучение модели
Теперь забудем что мы делали данные сами, представим что нам их кто-то дал и велел сказать, какая зависимость между входом и выходом в этих данных.

Предположим, что зависимость линейная, тогда мы можем узнать коэффициенты этой зависимости с помощью линейной регрессии.

Процесс обучения линейной регрессии итерационный, на каждом шаге мы от старых значений параметров отнимаем величину, зависящую от производной функции ошибки по этому параметру. Но итерации надо с чего-то начать, т.е. у нас должны быть "старые" значения параметров еще до начала итераций. Указание таких "старых" значений называется **инициализацией**, параметры нужно инициализировать. Это одна из проблем градиентного спуска и итерационных методов вообще. От того как мы инициализируем параметры будет зависеть и скорость обучения и результат обучения и сможет ли модель вообще чему-то полезному обучиться. Когда не знаешь как лучше - делай случайно: инициализируем параметры случайными числами из нормального распределения.

В линейной регрессии для одномерных величин мы имеем два параметра: коэффициент (множитель) **a**  и смещение (слагаемое) **b**. 
"""

# %%
a =  np.random.randn(1)# случайное число для параметра a
a

# %%
b = np.random.rand(1)# случайное число для параметра b 
b

# %%
"""
Обучать будем методом наименьших квадратов, для него нужно задать несколько гиперпараметров: шаг обучения  и количество итераций обучения, назовем их эпохами.
"""

# %%
lr = 10e-3 # шаг обучения
epochs = 100 # количество эпох


# %%
# ЦИКЛ ОБУЧЕНИЯ
Loss=[]# массив для значений функции ошибки
for ep in range(epochs): # в цикле по количеству эпох
    y_pred = b + a*x_train # считаем выход модели для всех примеров входов с текущими значениями параметров модели
    error = (y_pred - y_train) # считаем разницу между полученным выходом и тем, который должен был быть
    
    loss = (error**2).mean() # считаем суммарную ошибку. Функция среднего mean() как замена суммирования по всем примерам 
    Loss.append(loss) # добавлем текущее значение в массив
    b_grad = 2 * error.mean() # считаем производную по параметру, смещению b (на 2 можно не умножать включив его в lr)
    a_grad = 2 * (x_train * error).mean() # считаем производную по параметру, множителю a
    
    # обучение = изменение параметров
    a = a - lr*a_grad # изменяем параметр a
    b = b - lr*b_grad # изменяем параметр b      
    
    if ep % 20 == 0 or ep==epochs-1: # каждые 20 эпох будем
        print('ep: %3d  loss: %8.6f   a=%4.3f  b=%4.3f'%(ep,loss,a,b)) # печатать значение функции ошибки

# %%
# посмотрим на функцию ошибки
from matplotlib import pyplot as plt  #
plt.plot(np.arange(epochs), Loss, color='green') # рисуем
plt.yscale('log') # логарифмический масштаб для наглядности

# %%
"""
Запускайте цикл обучения несколько раз, смотрите, как обучается модель.

В итоге, мы пришли к *почти* тем же самым параметрам, которые использовались при создании данных, т.е. установили зависимость. Проверим на тестовых данных.

Сначала рассчитаем выходы модели на тестовых примерах входов `x_test`.
У нас уже были соответствующие желаемые тестовые выходы для них в `y_test`.
Построим их.
"""

# %%
y_test_pred = b + a*x_test # рассчитаем выходы модели на тестовых примерах входов
#у нас уже были соответствующие желаемые тестовые выходы для них в y_test

# %%
# нарисуем
import matplotlib.pyplot as plt
plt.scatter(x_test, y_test,  color='black')
plt.plot(x_test, y_test_pred, color='blue', linewidth=3)
plt.show()

# %%
"""
Получилось довольно точное приближение, значит мы обучили линейную регрессию правильно.
### Библиотека `sklearn`
Давайте сделаем тоже самое с помощью библиотеки. Будем использовать библиотеку [`sklearn`](https://scikit-learn.org/stable/) в которой реализовано множество моделей, в том числе и линейная регрессия.  
Модели вообще и линейная регрессия в частности реализуются специальными классами для которых реализованы методы обучения, расчета выхода, ошибки и другие функции. Линейная регрессия представлена в [`LinearRegression`](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LinearRegression.html). 

Давайте подключим эту модель из `sklearn.linear_model` и создадим объект такого класса.

Для модели линейной регрессии реализовано несколько методов:
* `fit()` -  для обучения моделей. Мы указываем массив примеров входов, массив примеров желаемых выходов (указаний учителя) и возможно дополнительные аргументы. Создает дополнительно поля `coef_` для хранения множителей **a** и `intercept_` для смещений **b**. (изначально таких полей нет).
* `predict()` - расчет выходов модели при заданных входах и сохраненных в модели параметрах
* `score()` - расчет значения функции ошибки для заданных примеров
* `get_params()`, `set_params()` - для чтения и установки параметров модели.


В `sklearn`, да и во многих других библиотеках, принято, чтобы в массивах примеры были первым измерением, а признаки - вторым. Для регрессии одномерной величины на 70 примерах, массив примеров должен быть размером 70 на 1. За этим нужно внимательно следить, если мы перепутаем порядок измерений и подадим массивы размера 1 на 70, то вычислительной ошибки не будет, но мы решим не ту задачу: вместо регрессии одномерной величины на 70 примерах, мы найдем регрессию 70-мерной величины на одном примере. Поэтому мы принудительно переформатируем размер массивов с помощью `reshape()`. Очевидно количество примеров в массивах входа и желаемого выхода должно быть одинаковым. 

*Примечание: По-видимому `sklearn` использует другой метод для обучения (на основе решения системы линейных уравнений), а не градиентный спуск, и немного другую функцию ошибки, но результат тот-же. Поэтому здесь не задается ни начальное значение параметров (инициализация), ни величина шага обучения. Это допустимо для линейной регрессии, но для более сложных случаев так не получится.*  

"""

# %%
from sklearn.linear_model import LinearRegression # подключим модель линейной регрессии
linr = LinearRegression() # создадим ее
linr.fit(x_train.reshape(70, 1), y_train.reshape(70, 1)) # обучим модель, принудительно переформатировав размеры массивов
print('Обученные параметры: b=%6.4f a=%6.4f'%(linr.intercept_, linr.coef_)) #

y_test_pred=linr.predict(x_test.reshape(30,1))# посчитаем выход модели
plt.plot(x_test, y_test_pred, color='blue', linewidth=3)# нарисуем его 
plt.scatter(x_test, y_test,  color='black') # и желаемый
plt.show()


# %%
"""
Давайте теперь попробуем сделать линейную регрессию на каких-то более-менее реальных данных из примеров `sklearn`. В этом наборе для классификации содержатся 3 класса примеров, количество примеров 59,71,48 для соответствующих классов (всего 178 примеров), каждый пример это 13-мерный вектор из действительных положительных чисел и номер класса.

Нам пригодятся вспомогательные функции `sklearn`:
* `train_test_split()` - случайно разделяющая данные на обучающие и тестовые в заданных долях. Подключаем ее из `sklearn.model_selection`


"""

# %%
from sklearn.datasets import load_wine # функция которая загружает набор данных 

# %%
data_wine = load_wine() # загружаем набор данных
data_wine.target[[10, 80, 140]] # в поле target хранятся номера классов примеров, некоторые из них приведены

# %%
list(data_wine.target_names) # названия классов в поле target_names

# %%
X = data_wine.data # в поле data - примеры входов, вектора
y = data_wine.target # Указания учителя = номера классов

from sklearn.model_selection import train_test_split # подключаем функцию для разделения данных
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3) # и отдаем 30% на тест, остальное на обучение
#y_train # убедитесь что и в обучении и в тесте есть данные разных классов
y_test #

# %%
X_train[:3] # первые три примера входов обучающих данных 

# %%
lin_clf = LinearRegression() # создаем модель линейной регресии
lin_clf.fit(X_train, y_train) # обучаем ее
print(f'Обученные параметры: \nСмещение b={lin_clf.intercept_:6.4f} ')
print('Множители a= ',lin_clf.coef_)

# %%
"""
Давайте разберемся, что же модель нам предсказала.

Для расчета выходов модели используем `predict()` на тестовых данных. А чтобы посчитать ошибку между действительным и желаемым выходом - `score()` (это приведенная ошибка в диапазоне от 0 до 1).

Посмотрим на визуализацию. Мы не можем рисовать 13-мерные графики, поэтому нарисуем их одномерные проекции. 
"""

# %%
y_predicted = lin_clf.predict(X_test) # рассчитываем выходы модели на тестовых данных
score=lin_clf.score(X_test,y_test)# считаем ошибку
print(score)#
i=0 # номер измерения для построения
plt.scatter(X_test[:,i], y_test,  color='black')# строим желаемые выходы 
plt.scatter(X_test[:,i], y_predicted, color='blue', linewidth=3)# и рассчитанные

# %%
"""
Не очень-то хорошее приближение.

Но вообще-то мы решаем задачу классификации, а не регрессии. Надо перевести выходы в номера классов. Для этого округлим значения выходов до ближайшего целого, если результат меньше 0, то установим 0, если больше 2, то установим 2. Сравним с указаниями учителя и посчитаем долю правильных (совпадающих) примеров. Найдем их долю.

Итак, хоть ошибка регрессии визуально большая, но для задачи классификации этих данных линейной регрессии достаточно. 
"""

# %%
y_pred_class=np.rint(y_predicted)
y_pred_class[y_pred_class>2]=2
y_pred_class[y_pred_class<0]=0
y_pred_class=y_pred_class.astype('int')
score=np.sum(y_pred_class==y_test)/len(y_test)
score

# %%
"""
Линейная регрессия очень простая модель, может моделировать только линейные зависимости. Не устойчива к *выбросам* - данным, которые резко выбиваются из примерной зависимости остальных данных (например, из-за ошибки измерения). Попробуйте в предыдущих примерах обучить линейную регрессию, на тех же данных, что были в начале нашего урока (почти линейные) но один обучающий пример испортите - сделайте для него выход скажем равным 100. И попробуйте снова обучить модель.      
"""

# %%
"""
# Логистическая регрессия
Логистическая регрессия похожа на линейную, но выход ее это не просто сумма произведений параметров и входов со смещением, а некоторая функция от этого. Применяют функцию под названием "сигмоида" или "логистическая функция", отсюда и название модели.

Сигмоида это функция вида: \\(f(x)= \frac{1}{1+e^{-x}}\\)

Нарисуем ее график, позаимствовав эту функцию, назвали `expit()`, из библиотеки [`scipy.special`](https://docs.scipy.org/doc/scipy/reference/special.html)  
"""

# %%
import matplotlib.pyplot as plt
from scipy.special import expit # подключаем функцию из библиотеки
x = np.linspace(-6, 6, 121) # диапазон по х
y = expit(x) # значение функции по у
plt.plot(x, y) #
plt.grid() #
plt.xlim(-6, 6)#
plt.xlabel('x')#
plt.title('expit(x)')#
plt.show()#

# %%
"""
Как нетрудно убедиться значения этой функции всегда лежат в пределах от 0 до 1, в нуле принимает значение 0.5. Если бы мы просто в линейной регрессии добавили эту функцию, то никогда не смогли бы моделировать зависимости, у которых выходная переменная больше 1 или меньше 0. Поэтому в логистической регрессии требуют, чтобы и указания учителя были в диапазоне 0...1.

Выход же логистической регрессии: 

\\(\overrightarrow{y}=f(A*\overrightarrow{x}+\overrightarrow{b})\\), 

где сигмоида применяется поэлементно к каждому компоненту вектора.

Для задач классификации, можно существенно изменить функцию ошибки. Давайте будем рассматривать только бинарную (двухклассовую) классификацию, и будем один класс обозначать величиной 0, второй - величиной 1. Тогда выход логистической регрессии (он от 0 до 1) можно интерпретировать как уверенность модели в том, что вход принадлежит к одному из классов. Выход ближе к нулю - принадлежит к классу 0, выход ближе к 1 - принадлежит классу 1.

Чтобы понять функцию ошибки будем рассуждать так:

Давайте посчитаем, насколько **правдоподобно**, что выход описывает заданный класс: 
* если класс должен быть 0, то правдоподобие для него это единица минус значение выхода модели y (должно быть 0, а получилось y, например выход у=0.3 - правдоподобие 1-0.3=0.7 - правдоподобно, большая уверенность в том, что класс 0; выход у=0.9 - правдоподобие 1-0.9=0.1 - совершенно не правдоподобно, очень маленькая уверенность, что класс 0)
* если класс должен быть 1, то правдоподобие для него это само значение выхода модели y (должно быть 1, а получилось y, например выход у=0.3 - правдоподобие = 0.3 - совершенно не правдоподобно, очень маленькая уверенность, что класс 1; выход у=0.9 - правдоподобие = 0.9 - правдоподобно, большая уверенность в том, что класс 1).

Выше приведены рассуждения для одного примера, если их несколько, то найдем произведение правдоподобий для каждого примера.

Например:
* для хорошего классификатора: получились выходы 0.1, 0.9, 0.8, а должны были получиться 0, 1, 1. Общее правдоподобие (1-0.1) * 0.9 * 0.8 = 0.648 довольно большое;
* для плохого классификатора: получились выходы 0.4, 0.6, 0.5, а должны были получиться 0, 1, 1. Общее правдоподобие (1-0.4) * 0.6 * 0.5 = 0.18 существенно меньше;
* для классификатора-врунишки, который все классы путает: получились выходы 0.7, 0.2, 0.1, а должны были получиться 0, 1, 1. Общее правдоподобие (1-0.7) * 0.2 * 0.1 = 0.006 очень маленькое.

Но работать с произведением маленьких чисел, когда их много, очень сложно, поэтому от значений правдоподобия переходят к его логарифму. Тогда числа по модулю уже не будут маленькими (но будут отрицательными), а произведение превратится в сумму (вспомните свойства логарифма).

Значения, которые мы подставляли для правдоподобия зависят от того, какой класс должен был получиться. Чтобы эти условия "если..." реализовать в одной формуле, можно поступить так:
- когда класс равен 0 нам нужно использовать \\(ln(1-y)\\), когда класс равен 1 нам нужно использовать  \\(ln(y)\\).
- запишем такую формулу: \\(t*ln(y)+(1-t)*ln(1-y)\\), где t (0 или 1) наш класс.   
- тогда при t=0 остается второе слагаемое \\(ln(1-y)\\), а при t=1 остается только первое слагаемое \\(ln(y)\\), как мы и хотели.

Итак, общее правдоподобие нашей модели:

$ E=\sum_{i=1}^{P} (t(x_i)*ln(y(x_i))+(1-t(x_i))*ln(1-y(x_i))\\
x_i : i-ый\ пример\ входа\\
t(x_i) : указание\ учителя\ для\ него\\
y(x_i) : выход\ модели\ для\ него \\ $

Правдоподобие нужно максимизировать, поменяем знак, можно минимизировать. Функция ошибки здесь это минус правдоподобие модели. Похожую формулу можно записать и для случая, когда классов больше чем два, но сейчас мы не будем этого делать. 

В функцию ошибки часто добавляют дополнительные члены, регуляризаторы, которые вводят некоторые ограничения, например чтобы величина параметров была не очень большая. Это бывает и полезно и вредно для обучения. Мы не будем трогать параметры по умолчанию, пока не придется.

Обучается такая модель методами на основе градиентного спуска (чистым градиентным спуском пользуются редко, обычно используют всякие его модификации, в которых, например, могут управлять величиной шага обучения или др.). Какой именно метод используется здесь для наших уроков не принципиально. 

В `sklearn` логистическая регрессия задается с помощью [`LogisticRegression()`](https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html#sklearn.linear_model.LogisticRegression) у которой есть такие же методы как и у `LinearRegression()` и некоторые специфичные аргументы:
* tol - точность до которой нужно обучать модель
* C - 1/коэффициент важности регуляризуещего члена в функции ошибки
* penalty -  вид регуляризации, 'none' если хотим отключить регуляризацию (по умолчанию включена)
* solver - название метода обучения, не будем трогать.
* max_iter - максимальное количество итераций для обучения

*Примечание: формулы приведенные для [`LogisticRegression`](https://scikit-learn.org/stable/modules/linear_model.html#logistic-regression) в документации, отличаются от наших, поскольку они выведены для случая когда желаемый выход +1 или -1, а в наших 0 либо 1.* 

Для логистической регрессии метод `predict()` возвращает классы (0 или 1 в нашем случае).

Чтобы вернуть выход используем метод `predict_proba()`. Он вернет величины правдоподобия для класса 0 (единица минус выход) и 1 (выход). 


"""

# %%
# Code source: Gael Varoquaux
# License: BSD 3 clause

import numpy as np #
import matplotlib.pyplot as plt #

from sklearn import linear_model #
from scipy.special import expit # функция для сигмоиды

# создадим данные: два отрезка на уровне 0 и 1

n_samples = 100 # число примеров
np.random.seed(0) # инициализация генератора случайных чисел
X = 1*np.random.normal(size=n_samples) # случаные числа по х, с нормальным законом распределения
y = (X > 0).astype(np.float) # класс 1 если больше нуля, 0 иначе, преобразуем в тип float
X[X > 0] *= 4 # умножим примеры входов из класса 1 на 4
X += .3 * np.random.normal(size=n_samples) # добавим ко всем примерам входа шум - случаные числа не очень большого разброса

X = X[:, np.newaxis] # сделаем столбец, добавив новое измерение к массиву, был одномерный станет двумерный. Можно было бы использовать reshape как раньше

# Обучение классификатора
log_reg = linear_model.LogisticRegression(C=1e5, max_iter=200) # создаем логистическую регрессию, C ставим побольше, это значит регуляризация будет влиять мало на результат
log_reg.fit(X, y) # обучаем модель, мы не стали разделять на обучающие и тестовые данные сейчас.

# рисуем результат
plt.figure(1, figsize=(4, 3)) # 
plt.clf() #
plt.scatter(X.ravel(), y, color='black', zorder=20) # ravel() вытягивает массив, превращая его в одномерный

X_test = np.linspace(-5, 10, 300) # вот теперь создадим тестовые примеры входов

# посчитаем выход модели
loss = expit(X_test * log_reg.coef_ + log_reg.intercept_).ravel() # вручную
loss1 = log_reg.predict_proba(X_test[:, np.newaxis])[:,1].ravel() # через predict_proba, берем второй столбец (индекс 1) , чтобы сам выход вернуть
plt.plot(X_test, loss1, color='red', linewidth=3) #

# сравним с линейной регрессией
ols = linear_model.LinearRegression() # линейная регрессия
ols.fit(X, y) # обучаем ее
plt.plot(X_test, ols.coef_ * X_test + ols.intercept_, linewidth=1)

# рисуем
plt.axhline(.5, color='.5') #

plt.ylabel('y') #
plt.xlabel('X') #
plt.xticks(range(-5, 10)) #
plt.yticks([0, 0.5, 1]) #
plt.ylim(-.25, 1.25) #
plt.xlim(-4, 10) #
plt.legend(('Logistic Regression Model', 'Linear Regression Model'), #
           loc="lower right", fontsize='small') #
plt.tight_layout() #
plt.show() #

# %%
"""
## Понятие разделяющей поверхности
Выше мы рассмотрели линейную и логистическую регрессии. Если мы используем  их для классификации, то вводится некоторое правило, как относить выходы к классам, например для двух классов для линейной регрессии можно считать так: если выход меньше нуля, то первый класс, если больше - второй. В логистической регрессии принято выбирать такой класс, для которого уверенность максимальна. Как именно относить выходы к классам это наш выбор, в некоторых библиотеках реализована возможность задать порог для такого отнесения, в некоторых нет, тогда используются наперед заданные значения. 

Давайте зададимся таким вопросом. Пусть решаем задачу бинарной классификации, есть примеры входов в двумерном пространстве, и мы обучили на них линейную и логистическую регрессии. Если мы, после обучения, проверим классы выдаваемые этими моделями для всех точек двумерной плоскости, то какую картину мы увидим? Давайте нарисуем.
"""

# %%
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model #

np.random.seed(42) # начало генератора случайных чисел, чтобы после перезапуска были одинаковые
N=100 # число точек в одном классе

# первый класс, случайные точки вокруг (-20, -20) выход = -1
x1 = -20+5*np.random.randn(N, 1) #
y1 = -20+5*np.random.randn(N, 1) #
z1=-1*np.ones((100,1)) #
# второй класс, случайные точки вокруг (+20, +20) выход = +1
x2 = 20+5*np.random.randn(N, 1) #
y2 = 20+5*np.random.randn(N, 1) #
z2=+1*np.ones((100,1)) #

# большой выброс, раскомментируйте для теста
#x1[0]=+1000
#y1[0]=+1000

# Объединяем массивы в один 
Input=np.hstack((np.vstack((x1, x2)),np.vstack((y1 , y2))))
Output=np.vstack((z1,z2))
#print(Input.shape)
#print(Output.shape)

fig, ax = plt.subplots()

# рисуем обучающие точки своим цветом
ax.scatter(Input[0:N-1,0],Input[0:N-1,1],color='green')
ax.scatter(Input[N:,0],Input[N:,1],color='blue')

# создаем и обучаем линейную регрессию
linr = linear_model.LinearRegression() # создадим ее. normalize=False если хотим без нормализации
linr.fit(Input, Output) # обучим модель, принудительно переформатировав размеры массивов
print('Обученные параметры b={d[0]} a={d[1]}'.format(d=(linr.intercept_, linr.coef_))) #

out_pred=linr.predict(Input)# посчитаем выход модели
# Если выход меньше нуля - принимаем за первый класс, если больше - за второй
# найдем ошибки, т.е. не совпадение между истинными классами и полученными моделью 
err1=(out_pred>0) & (Output==-1)# думаем что второй а на самом деле первый класс
err2=(out_pred<0) & (Output==+1)# думаем что первый а на самом деле второй класс
err1=err1[:,0]
err2=err2[:,0]

# нарисуем ошибки
ax.scatter(Input[err1,0],Input[err1,1],color='yellow',marker='x')
ax.scatter(Input[err2,0],Input[err2,1],color='magenta',marker='x')

# возьмем много точек плоскости
h=0.2
x_min,y_min=Input.min(axis=0)-10
x_max, y_max=Input.max(axis=0)+10
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h)) # точки плоскости из равномерной сетки
Input_test=np.c_[xx.ravel(), yy.ravel()]
#print(Input.shape)

# посчитаем для них выходы обученной линейной регрессии
zz = linr.predict(Input_test)
zz = zz.reshape(xx.shape)
zz[zz>0]=1 # больше нуля - второй класс
zz[zz<0]=-1 # меньше нуля - первый класс
ax.contourf(xx, yy, zz, cmap=plt.cm.coolwarm, alpha=0.8); # рисуем цветом выходы для всех точек плоскости

# рисуем прямую
xxx=np.arange(x_min,x_max,h)
yyy=-(linr.intercept_[0] + linr.coef_[0][0]*xxx)/linr.coef_[0][1] # x2=-(a1*x1+b)/a2

ax.plot(xxx,yyy,color='black');
ax.set_xlim([-40, 40])
ax.set_ylim([-40, 40])

# %%
"""
Мы видим, что плоскость разделилась на две области, в одной все точки приняты за первый класс, в другой - за второй. Если присмотреться, то граница раздела между этими областями это **прямая линия**. Какие бы данные вы не подавали, как бы не обучали линейную регрессию, она всегда будет разбивать плоскость на две части - полуплоскости (для бинарной классификации). Аналогично в многомерном случае, но теперь граница раздела будет не прямой линией, а гиперплоскостью. Такая граница называется **разделяющая поверхность** и для линейной регрессии является гиперплоскостью. Можете убедиться в этом, ведь уравнение этой поверхности - границы раздела - это условие что выход равен нулю. Подставьте в уравнение выхода линейной регрессии и увидите, что получилось уравнение гиперплоскости. **В линейной регрессии разделяющая поверхность - линейная**, ее можно нарисовать.

А что для логистической регрессии? Постройте аналогично, и увидите, это для нее, хотя выход логистической регрессии нелинеен по входам, разделяющая поверхность тоже линейная. Сама поверхность может отличаться от поверхности для линейной регрессии, но все равно она является линейной. 
"""

# %%
import numpy as np
import matplotlib.pyplot as plt
from sklearn import linear_model #

np.random.seed(42) # начало генератора случайных чисел, чтобы после перезапуска были одинаковые
N=100

x1 = -20+5*np.random.randn(N, 1) #
y1 = -20+5*np.random.randn(N, 1) #
z1=0*np.ones((100,1)) # классы должны быть 0 или 1

x2 = 20+5*np.random.randn(N, 1) #
y2 = 20+5*np.random.randn(N, 1) #
z2=1*np.ones((100,1)) # классы должны быть 0 или 1

Input=np.hstack((np.vstack((x1, x2)),np.vstack((y1 , y2))))
Output=np.vstack((z1,z2))
#print(Input.shape)
#print(Output.shape)

fig, ax = plt.subplots()

ax.scatter(Input[0:N-1,0],Input[0:N-1,1],color='green')
ax.scatter(Input[N:,0],Input[N:,1],color='blue')

logr = linear_model.LogisticRegression() # 
logr.fit(Input, Output.ravel()) # обучим модель, 
print('Обученные параметры b={d[0]} a={d[1]}'.format(d=(logr.intercept_, logr.coef_))) #
out_pred=logr.predict(Input)# посчитаем выход модели
out_pred

# в логистической регресии predict возвращает классы, так что не надо самим относить выходы к классам
# больше 0.5 класс 1, меньше 0.5 класс 0.
err1=(out_pred==1) & (Output==0)
err2=(out_pred==0) & (Output==1)
err1=err1[:,0]
err2=err2[:,0]

ax.scatter(Input[err1,0],Input[err1,1],color='yellow',marker='x')
ax.scatter(Input[err2,0],Input[err2,1],color='magenta',marker='x')

h=0.2
x_min,y_min=Input.min(axis=0)-10
x_max, y_max=Input.max(axis=0)+10
xx, yy = np.meshgrid(np.arange(x_min, x_max, h), np.arange(y_min, y_max, h))
Input_test=np.c_[xx.ravel(), yy.ravel()]
#print(Input.shape)

zz = logr.predict(Input_test)
zz = zz.reshape(xx.shape)
# в логистической регресии predict возвращает классы, так что не надо самим относить выходы к классам
#zz[zz>0]=1
#zz[zz<0]=-1
ax.contourf(xx, yy, zz, cmap=plt.cm.coolwarm, alpha=0.8);

xxx=np.arange(x_min,x_max,h)
# все равно это прямая линия, но возможно с другими коэффициентами
yyy=-(logr.intercept_[0] + logr.coef_[0][0]*xxx)/logr.coef_[0][1]

ax.plot(xxx,yyy,color='black');

# %%
"""
Для случаев трех и более классов, разделяющая поверхность будет кусочно-линейной, состоять из частей гиперплоскостей. Постройте самостоятельно для случая трех классов.

Чтобы в линейной регрессии закодировать три и более класса, можно поступить так: давайте сделаем выход многомерным, размерность его равна числу классов; пусть все элементы вектора указаний учителя равны нулю кроме одного. Элемент вектора указаний учителя с номером совпадающем с номером класса равен 1. Такое кодирование называется one-hot кодирование (иногда бинарное или унитарное кодирование). Например для трех классов можно закодировать указания учителя так: первый класс - вектор (1, 0, 0);  второй класс - вектор (0, 1, 0); третий класс - вектор (0, 0, 1). Для линейной регрессии можно использовать и другие числа, например: первый класс - вектор (+1, -1, -1);  второй класс - вектор (-1, +1, -1); третий класс - вектор (-1, -1, +1) или аналогичные. Для логистической регрессии значения выходов и указаний учителя должны быть от 0 до 1. 

Относить выход к классу можно, как вариант, по максимальной близости вектора выхода к заданным векторам классов, например: выход (0.9, 0.1, 0.1) ближе всех к (1, 0, 0), а значит относится к первому классу. Можно придумать и другие способы. 

Вопрос, а как бы вы интерпретировали выход (0.5, 0.5, 0) ? 
"""

# %%
"""
# Обсуждение и задания
Посчитайте самостоятельно сколько правильно и неправильно угаданных классов, если считать истинный класс 0 если X меньше и класс 1 если больше. Сравните количество ошибок для логистической и линейной регрессии. Для линейной регрессии считать класс 1 если выход больше 0.5 и 0 если меньше. 

Придумайте данные, для которых и логистическая регрессия будет работать плохо и покажите это.

Посмотрите как влияет аргумент max_iter на качество регрессии.

Онлайн визуализацию для линейных моделей можно найти здесь 

http://vision.stanford.edu/teaching/cs231n-demos/linear-classify/ 
"""